console.log('No. 1 Looping While :');
console.log('');
console.log('LOOPING PERTAMA');
var flag = 2;
while(flag <= 20) { 
  console.log(flag+' - I love coding'); 
  flag = flag + 2; 
}

console.log('LOOPING KEDUA');
var flag = 20;
while(flag >= 2) { 
  console.log(flag+' - I will become a mobile developer'); 
  flag = flag - 2; 
}

console.log('');
console.log('No. 2 Looping menggunakan for :');
console.log('');
for(var angka = 1; angka < 21; angka++) {
 if (angka%2==0) {
  console.log(angka + ' - Berkualitas'); }
 else {
 if (angka%3==0) {
  console.log(angka + ' - I Love Coding'); }
 else { console.log(angka + ' - Santai'); }
 }
} 

console.log('');
console.log('No. 3 Membuat Persegi Panjang # :');
console.log('');
for(var angka = 1; angka < 5; angka++) {
 console.log('########');
}

console.log('');
console.log('No. 4 Membuat Tangga :');
console.log('');
var tk = '#';
var tk2 = '';
for(var angka = 1; angka < 8; angka++) {
 tk2 = '';
 for(var b = 1; b <= angka; b++) {
  tk2 = tk2 + tk; 
 }
 console.log(tk2);
}

console.log('');
console.log('No. 5 Membuat Papan Catur :');
console.log('');
for(var angka = 1; angka < 9; angka++) {
  if (angka%2==0) {
     console.log('# # # #'); }
  else {
     console.log(' # # # #'); }
}
