console.log("1. Mengubah fungsi menjadi fungsi arrow");
console.log("");
const golden = goldenFunction = () => { 
  console.log("this is golden!!")
}
 
golden()

console.log("");
console.log("");
console.log("2. Sederhanakan menjadi Object literal di ES6");
console.log("");
const newFunction = literal = (firstName, lastName) => {
  return {
    fullName: () => {
      console.log(firstName + " " + lastName)
      return 
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName()

console.log("");
console.log("");
console.log("3. Destructuring");
console.log("");
let newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
} 

const {firstName,lastName,destination,occupation,spell} = newObject;
console.log(firstName, lastName, destination, occupation);

console.log("");
console.log("");
console.log("4. Array Spreading");
console.log("");
let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east]
//Driver Code
console.log(combined)


console.log("");
console.log("");
console.log("5. Template Literals");
console.log("");
const planet = 'earth'
const view = 'glass'
var before = 'Lorem ${view} dolor sit amet, ' +  
    'consectetur adipiscing elit,${planet} do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 
// Driver Code
console.log(before) 
