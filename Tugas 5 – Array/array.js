console.log("Soal No. 1 (Range) :");
function range(startNum, finishNum) {
  if (startNum == null || finishNum == null) {
   return -1;
 }
 else {
  var bil = [];
  if (startNum<finishNum) {
   for(var angka = startNum; angka <= finishNum; angka++) {
     bil.push(angka);
   }
  }
  else {
   for(var angka = startNum; angka >= finishNum; angka--) {
     bil.push(angka);
   }    
  }
  return bil;
 }
}

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11,18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1

console.log("");
console.log("Soal No. 2 (Range with Step) :");

function rangeWithStep(startNum, finishNum, step) {
  if (startNum == null || finishNum == null) {
   return -1;
 }
 else {
  var bil = [];
  if (startNum<finishNum) {
   for(var angka = startNum; angka <= finishNum; angka=angka+step) {
     bil.push(angka);
   }
  }
  else {
   for(var angka = startNum; angka >= finishNum; angka=angka-step) {
     bil.push(angka);
   }    
  }
  return bil;
 }
}

console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5] 

console.log("");
console.log("Soal No. 3 (Sum of Range) :");
function sum(startNum, finishNum, step) {
 if (startNum == null && finishNum == null && step == null) {
  return 0;
 }
 else if (startNum != null && finishNum == null && step == null) {
  return startNum;
 }
 else if (startNum == null && finishNum != null && step == null ) {
  return finishNum;
 }
 else if (startNum == null && finishNum == null && step != null ) {
  return step;
 }
 else {
  var bil = [];
  if (step == null) {
    bil = range(startNum,finishNum);
  } 
  else {
    bil = rangeWithStep(startNum,finishNum,step);
  }
  
  var sumar = bil.reduce(function(a,b) { return a+b; },0);
  return sumar;
 }
}

console.log(sum(1,10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15,10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0 

console.log("");
console.log("Soal No. 4 (Array Multidimensi) :");
console.log("");
function dataHandling(inputarray) {
 var kalimat=""
 for(var angka = 0; angka < 4; angka++) {
     kalimat = kalimat + "Nomor ID:"+inputarray[angka][0]+"\n"+"Nama Lengkap:"+inputarray[angka][1]+"\n"+"TTL:"+inputarray[angka][2]+" "+inputarray[angka][3]+"\n"+"Hobby:"+inputarray[angka][4]+"\n\n";
 }
 return kalimat;
}

var inputar = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
              ] 

console.log(dataHandling(inputar));
