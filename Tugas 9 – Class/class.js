console.log("1. Animal Class");
console.log("");
console.log("Release 0");
console.log("");

class Animal {
  constructor(name) {
    this._name         = name;
    this.legs         = 4;
    this.cold_blooded = false;
  }

  get name() {
    return this._name;
  }

  set name(x) {
    this._name = x;
  }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log("");
console.log("Release 1");
console.log("");

class Ape extends Animal {
  constructor(name) {
    super(name);    
  }
  yell() {
    return 'Auooo';
  }
}

class Frog extends Animal {
  constructor(name) {
    super(name);    
  }
  jump() {
    return 'hop hop';
  }
}

var sungokong = new Ape("kera sakti")
console.log(sungokong.yell()) // "Auooo"
 
var kodok = new Frog("buduk")
console.log(kodok.jump()) // "hop hop"

