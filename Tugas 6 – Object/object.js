console.log("Soal No. 1 (Array to Object)");
console.log("");
function arrayToObject(arr) {  
  if (arr.length==0) {
    console.log("");
  }
  else { 
   var urutan = 0; 
   var now = new Date()
   var thisYear = now.getFullYear()
   for(var b = 0; b < arr.length; b++) {
    urutan = b+1;

    var objarr = {};
    var objarr2 = {};   

    objarr2.firstName = arr[b][0];
    objarr2.lastName  = arr[b][1];
    objarr2.gender    = arr[b][2];
    if (arr[b].length==3 || thisYear < arr[b][3]) { objarr2.age = "Invalid Birth Year";}
    else { objarr2.age = thisYear - arr[b][3]; }
    objarr[urutan+". "+arr[b][0]+" "+arr[b][1]] = objarr2;
    console.log(objarr);
   }
   }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""
/* console.log("Soal No. 2 (Shopping Time)");
console.log("");

var item = [["Sepatu brand Stacattu",1500000],["Baju brand Zoro",500000],["Baju brand",250000],["Sweater brand Uniklooh",175000],["Casing Handphone",50000]];

function shoppingTime(memberId, money) {
  if (memberId==null) {
   return "Mohon maaf, toko X hanya berlaku untuk member saja";
  } 
  else if (money<50000) {
   return "Mohon maaf, uang tidak cukup";
  }
  else {
   var shopobj = {};
   var listp = [];
   var smoney = money;
   var flag = 0;
   while(flag < item.length) { 
    if (item[flag][1]<smoney)
    flag++; 
   }

   shopobj.memberID = memberId;
   shopobj.money = money;
   shopobj.listPurchased = listp;
   shopobj.changeMoney = 
  }
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
*/
